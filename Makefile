PROJ_NAME := 'beejee-testme'

docker:
	docker-compose -f $$PWD/docker/docker-compose.yml -p ${PROJ_NAME} up

.PHONY: docker
.SILENT:
