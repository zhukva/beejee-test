<?php

require_once '../vendor/autoload.php';

use Dotenv\Dotenv;

$dotenv = new Dotenv('..');
$dotenv->load();

$app = new BJ\Application();

if(php_sapi_name() == 'cli') {
    return $app->cli();
} else {
    echo $app->serv();
}
