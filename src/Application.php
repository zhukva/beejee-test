<?php

namespace BJ;

use Phroute\Phroute\RouteCollector;
use Phroute\Phroute\Dispatcher;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Intervention\Image\ImageManager;

use BJ\Controllers\LoginController;
use BJ\Controllers\TaskController;

/**
* Main application
*/
class Application
{
    private $em;
    private $tpl;

    public function __construct()
    {
        session_start();
        // database
        $config = Setup::createAnnotationMetadataConfiguration([__DIR__ . '/Models'], true);
        $conn = array(
            'driver' => 'pdo_sqlite',
            'path' => getenv('SQLITE_DB_PATH') ?: __DIR__ . '/database.sqlite',
        );
        $this->em = EntityManager::create($conn, $config);

        // templates
        $loader =  new \Twig_Loader_Filesystem(__DIR__ . '/Views');
        $this->tpl = new \Twig_Environment($loader);
    }

    public function serv()
    {
        $router = new RouteCollector();
        $router->filter('auth', function () {
            if (!isset($_SESSION['user'])) {
                header('Location: /login');
                return false;
            }
        });

        // root
        $router->get('/', function () {
            header('Location: /tasks');
            return false;
        });

        // public area
        $router->get('/login', function () {
            return $this->tpl->render('login/login.html',[
                'title' => 'Авторизация'
            ]);
        });
        $router->post('/login', function () {
            $ctrl = new LoginController($this->em);
            $user = filter_input(INPUT_POST, 'login', FILTER_SANITIZE_STRING);
            $pass = filter_input(INPUT_POST, 'passwd', FILTER_SANITIZE_STRING);
            if ($name = $ctrl->checkAuth($user, $pass)) {
                $_SESSION['user'] = $name;
            }
            header('Location: /tasks');
        });
        $router->get('/logout', function () {
            unset($_SESSION['user']);
            header('Location: /tasks');
        });
        $router->get('/tasks', function () {
            $ctrl = new TaskController($this->em);

            $page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT);
            $sort = filter_input(INPUT_GET, 'sort', FILTER_SANITIZE_STRING);

            if (!empty($sort)) {
                $prevpage_url['sort'] = $sort;
                $nextpage_url['sort'] = $sort;
            }

            $lastpage = ceil($ctrl->count() / 3);
            if ($page < 2) {
                $page = 1;
                $prevpage_url = [];
                if ($lastpage < 1) {
                    $nextpage_url = [];
                } else {
                    $nextpage_url['page'] = $page + 1;
                }
            } else {
                $prevpage_url['page'] = $page - 1;
                if ($page == $lastpage) {
                    $nextpage_url = [];
                } else {
                    $nextpage_url['page'] = $page + 1;
                }
            }

            $tasks = $ctrl->list($page, $sort);
            return $this->tpl->render('task/list.html',[
                'title' => 'Список задач',
                'user' => isset($_SESSION['user']) ? $_SESSION['user'] : null,
                'tasks' => $tasks,
                'nextpage' => http_build_query($nextpage_url) ? '/tasks?' . http_build_query($nextpage_url) : null,
                'prevpage' => http_build_query($prevpage_url) ? '/tasks?' . http_build_query($prevpage_url) : null
            ]);
        });
        $router->get('/tasks/new', function () {
            return $this->tpl->render('task/newform.html',[
                'title' => 'Новая задача'
            ]);
        });
        $router->post('/tasks/new', function () {
            $ctrl = new TaskController($this->em);
            $data = [
                'user' => filter_input(INPUT_POST, 'user', FILTER_SANITIZE_STRING),
                'email' => filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL),
                'task' => filter_input(INPUT_POST, 'task', FILTER_SANITIZE_STRING)
            ];
            if (isset($_FILES['image']) && $_FILES['image']['error'] == UPLOAD_ERR_OK) {
                try {
                    $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                    $name = md5($_FILES['image']['name']);
                    $path_saveto = sprintf('/public/%s.%s', $name, $ext);
                    $mgr = new ImageManager(['driver' => 'gd']);
                    $mgr->make($_FILES['image']['tmp_name'])->fit(320, 240)->save(sprintf('%s%s', $_SERVER['DOCUMENT_ROOT'], $path_saveto));
                    $data['image'] = $path_saveto;
                } catch (\Exception $e) {
                    $data['image'] = null;
                }
            }
            $ctrl->newTask($data);
            header('Location: /tasks');
        });

        // secured area
        $router->group(['before' => 'auth'], function ($router) {
            $router->get('/tasks/{id}/edit', function ($id) {
                $ctrl = new TaskController($this->em);
                $item = $ctrl->task($id);
                return $this->tpl->render('task/editform.html',[
                    'title' => 'Редактирование задачи',
                    'item' => $item,
                    'id' => $id
                ]);
            });
            $router->post('/tasks/{id}/edit', function ($id) {
                $ctrl = new TaskController($this->em);
                $task = filter_input(INPUT_POST, 'taskText', FILTER_SANITIZE_STRING);
                $is_completed = filter_input(INPUT_POST, 'isCompleted', FILTER_SANITIZE_NUMBER_INT);
                $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
                $ctrl->saveTask($id, ['task' => $task, 'isCompleted' => $is_completed]);
                header('Location: /tasks');
            });
        });

        $dispatcher = new Dispatcher($router->getData());
        $r = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
        return $r;
    }

    public function cli()
    {
        return ConsoleRunner::createHelperSet($this->em);
    }
}
