<?php

namespace BJ\Models;


/**
* Task model
* @Entity @Table(name="tasks")
*/
class TaskModel
{
    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $user;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $email;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $task;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $image;

    /**
     * @Column(type="boolean", name="is_completed")
     * @var boolean
     */
    protected $isCompleted;

    public function getId()
    {
        return $this->id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getTask()
    {
        return $this->task;
    }

    public function setTask($task)
    {
        $this->task = $task;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image_path)
    {
        $this->image = $image_path;
    }

    public function getIsCompleted()
    {
        return $this->isCompleted;
    }

    public function setIsCompleted($state)
    {
        $this->isCompleted = $state;
    }
}
