<?php

namespace BJ\Models;


/**
* User model
* @Entity @Table(name="users")
*/
class UserModel
{
    /**
     * @Id @Column(type="integer") @GeneratedValue
     * @var int
     */
    protected $id;
    /**
     * @Column(type="string")
     * @var string
     */
    protected $name;
    /**
     * @Column(type="string")
     * @var string
     */
    protected $passwd;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getPassword()
    {
        return $this->passwd;
    }

    public function setPassword($passwd)
    {
        $this->passwd = $passwd;
    }
}
