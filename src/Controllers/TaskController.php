<?php

namespace BJ\Controllers;

use BJ\Models\TaskModel;


/**
* Task controller
*/
class TaskController extends AbstractController
{
    public function list($page = 1, $sort = null)
    {
        $offset = (($page - 1) * 3);
        if ($sort) {
            $sort = [$sort => 'ASC'];
        }
        try {
            return $this->em()->getRepository(TaskModel::class)->findBy([], $sort, 3, $offset);
        } catch (\Exception $e) {
            return [];
        }
    }

    public function count()
    {
        return count($this->em()->getRepository(TaskModel::class)->findAll());
    }

    public function task($id)
    {
        try {
            return $this->em()->getRepository(TaskModel::class)->findOneBy(['id' => $id]);
        } catch (Exception $e) {
            return null;
        }

    }

    public function saveTask($id, $data)
    {
        $task = $this->task($id);
        if ($task) {
            $task->setTask($data['task']);
            $task->setIsCompleted(isset($data['isCompleted']));
            $this->em()->persist($task);
            $this->em()->flush();
        }
    }

    public function newTask($data)
    {
        $task = new TaskModel();
        $task->setUser($data['user']);
        $task->setEmail($data['email']);
        $task->setTask($data['task']);
        $task->setImage(isset($data['image']) ? $data['image'] : null);
        $task->setIsCompleted(false);
        $this->em()->persist($task);
        $this->em()->flush();
    }
}
