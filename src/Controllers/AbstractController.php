<?php

namespace BJ\Controllers;

use Doctrine\ORM\EntityManager;


/**
* Abstract controller
*/
abstract class AbstractController
{
    private $em;

    final public function __construct($em)
    {
        if ($em instanceof EntityManager) {
            $this->em = $em;
        } else {
            throw new \Exception('Entity manager should be instance of Doctrine\ORM\EntityManager');
        }
    }

    final public function em()
    {
        return $this->em;
    }
}
