<?php

namespace BJ\Controllers;

use BJ\Models\UserModel;


/**
* Login controller
*/
class LoginController extends AbstractController
{
    public function checkAuth($user, $passwd)
    {
        try {
            $persist_user = $this->em()->getRepository('BJ\Models\UserModel')->findOneBy(['name' => $user, 'passwd' => $this->encodePassword($passwd)]);
            if (!$persist_user) {
                throw new \Exception('User not found');
            }
            return $persist_user->getName();
        } catch (\Exception $e) {
            return false;
        }
    }

    private function encodePassword($value)
    {
        $options = [
            'cost' => 11,
            'salt' => getenv('PASSWD_SALT') ?: 'vbJK6IKgBnNyts78J/ReiFoPTPEGzumUJDik3gmA+c0='
        ];
        return password_hash($value, PASSWORD_BCRYPT, $options);
    }
}
